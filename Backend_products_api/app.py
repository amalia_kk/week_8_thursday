from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

# print(app)
# print(api)
# These are just to check

# Code for Flask API
# We need muffins to be supplied as JSON
class Product(Resource):
    def get(self):
        return {
            "products": ["Beer-induced", "Double chocolate", "Duffin", "Raisin"]
        }

class Product_price(Resource):
    def get(self):
        return {
            "price": [12, 10, 17, 10]
        }


# Expose this in the API
api.add_resource(Product, '/')
api.add_resource(Product_price, '/price')

if __name__ == '__main__':
    print("This is the main!")
    print("Use this block of code to make an app have different behaviour when called directly")
    app.run("0.0.0.0", port=80, debug=True)

