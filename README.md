# Vegan muffin store

This will be an app to showcase Docker Compose. We'll build it with  PHP html front page and another container with a python API simulating a database. 

We'll use this class to cover:

- Dev environment
- Testing and CD environment
- Git and github branching
  
We will also touch on:

- Python
- APIs
- Webapps
- Separation of concerns
  
Extras:

- Add CI
- Add CD
  
### Plan to complete

1. Create a simple API with python and put it in a container
2. Create simple PHP app to parse/consume JSON sent from API
3. Build our Docker compose

